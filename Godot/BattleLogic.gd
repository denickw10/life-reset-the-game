extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
enum ActionList {Attack,Specials,Magic,Items,Switch,Run}
export(NodePath) var ActionNodePath
export(NodePath) var EnemyNodePath
var ActionNode
var CurrentMode = "Actions"
var EnemyList = []
var isDead = false
signal PlayerTurn

# Called when the node enters the scene tree for the first time.
func _ready():
	ActionNode = get_node(ActionNodePath)
	for i in range(0,get_node(EnemyNodePath).get_child_count()):
		EnemyList.append(get_node(EnemyNodePath).get_child(0))
	ResetActionPanel()
	for i in EnemyList:
		i.get_child(3).max_value = i.MaxHealth
		i.get_child(3).value = i.Health


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

# Just Imcrement The Player Timer
func _on_Timer_BattleTime():
	if !isDead:
		$TopBar/PlayerTimer.value += $TopBar/PlayerTimer.step
	for i in EnemyList:
		if i.get_child(2).value == 100:
			$PlayerUI/HealthLabel/Health.value -= 10
			i.get_child(2).value = 0
			break
		i.get_child(2).value += i.get_child(2).step
	pass

# Just Signal That Its The Player Turn When Full
func _on_PlayerTimer_value_changed(value):
	if value >= 100:
		emit_signal("PlayerTurn")
		for i in range(0,ActionNode.get_item_count()):
			ActionNode.set_item_disabled(i ,false)

# Main Logic For The ItemList
func _on_PlayerAction_item_selected(index):
	# needed because disabling item does not prevent selection
	# this looks like a bug in the engine and should make a issue on their github later
	if ActionNode.is_item_disabled(index):
		return
# Check The Current Mode Of The ItemList
# So We Can Do Diffrent Things With The Same Node
	match CurrentMode:
		"Actions":
			match index:
				ActionList.Attack:
					CurrentMode = "Attack"
					ActionNode.clear()
					AttackList()
				ActionList.Specials:
					print("Special Selected")
				ActionList.Magic:
					print("Magic Selected")
				ActionList.Items:
					print("Item Selected")
				ActionList.Switch:
					print("Switch Selected")
				ActionList.Run:
					print("Run Selected")
				_:
					print("Error: Unknown Option Selected")
		"Attack":
			print("Enemy Selected: " + EnemyList[index].name)
			EnemyList[index].Health -= 1
			EnemyList[index].get_child(3).value = EnemyList[index].Health
			if EnemyList[index].get_child(3).value == 0:
				EnemyList[index].queue_free()
				EnemyList.remove(index)
			ResetActionPanel()
			$TopBar/PlayerTimer.value = 0 # Might Move This Into ResetActionPanel If This Get Repeated Alot
		_:
			print("Error: Unselectable Option For Enemy")
	ActionNode.unselect_all()
	
	
	
# Reset The ItemList 
# Reset It To The Normal Mode
func ResetActionPanel():
	ActionNode.clear()
	CurrentMode = "Actions"
	for i in ActionList:
		ActionNode.add_item(i)
		ActionNode.set_item_disabled(ActionNode.get_item_count() -1 ,true)
		
# Just Fills In The ItemList With All Of The Enemy
func AttackList():
	print("Attack Selected")
	for i in EnemyList:
		ActionNode.add_item(i.name)	

#Helper Function To Disable All Of The Item In The ItemList
func DisableActionPanel():
	for i in range(0,ActionNode.get_item_count()):
		ActionNode.set_item_disabled(i ,true)
# Does Nothing Right Now But Should Used To Allow Click On The Enemy Directly For Attackl Instead Of Using The ItemList
func _on_Enemy_mouse_entered():
	pass # Replace with function body.


func _on_Health_value_changed(value):
	if value <= 0:
		isDead = true
		print("You Have Dead")
